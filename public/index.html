<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <title>Collider Physics meets Gravitational Waves</title>
    
    <meta name="description" content="Presentation Slides made with impress.js" />
    <meta name="author" content="Gregor Kaelin" />

	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
      "HTML-CSS": {
	  scale: 90,
	  styles: {
      '.MathJax_Display': {
      "margin": "20px 0"
      }
      }
	  },
	  TeX: { extensions: ["color.js"] },
	  });
	</script>
	<script type="text/javascript" src="extras/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

    <link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  </head>

  <body class="impress-not-supported">

	<div class="fallback-message">
      <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
      <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
	  <p>
		$$
		\definecolor{desyOrange}{RGB}{242,142,0}
		\DeclareMathOperator{\Arcsinh}{arcsinh}
		\DeclareMathOperator{\Arctan}{arctan}
		\DeclareMathOperator{\arcosh}{arcosh}
		\newcommand\dd{{\mathrm d}}
		\newcommand{\bb}{{\mathbf b}}
		\newcommand{\bk}{{\mathbf k}}
		\newcommand{\bl}{{\mathbf l}}
		\newcommand{\bm}{{\mathbf m}}
		\newcommand{\bn}{{\mathbf n}}
		\newcommand{\bp}{{\mathbf p}}
		\newcommand{\bq}{{\mathbf q}}
		\newcommand{\br}{{\mathbf r}}
		\newcommand{\bw}{{\mathbf w}}
		\newcommand{\bx}{{\mathbf x}}
		\newcommand{\by}{{\mathbf y}}
		\newcommand{\bz}{{\mathbf z}}
		\newcommand{\bc}{{\mathbf c}}
		\newcommand{\bell}{\boldsymbol{\ell}}
		\newcommand\cO{\mathcal{O}}
		\newcommand{\cD}{\mathcal{D}}
		\newcommand\cA{\mathcal{A}}
		\newcommand\cM{\mathcal{M}}
		\newcommand\cN{\mathcal{N}}
		\newcommand\cE{\mathcal{E}}
		\newcommand\cS{\mathcal{S}}
		\newcommand\cP{\mathcal{P}}
		\newcommand\cF{\mathcal{F}}
		\newcommand\cL{\mathcal{L}}
		\newcommand\cH{\mathcal{H}}
		\newcommand\cU{\mathcal{U}}
		\newcommand\Mp{M_{\rm Pl}}
		$$
	  </p>
	</div>

	<div id="impress">

	  <div id="title" class="step vanishPast" data-x="0" data-y="0">
		<h1>C<span class="orange">o</span>llider Physics meets Gravitational Waves</h1>
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		<br class="margin-bottom80"/>
		<p id="author">
		  Gregor Kälin
		</p>
		<div class="margin-top90 margin-bottom30">
		  <img id="ercFig" src="figures/erc.svg" alt="erc">
		  <img id="desyFig" src="figures/logo_DESY.png" alt="desy">
		  <img id="euFig" src="figures/eu.svg" alt="eu">
		</div>
		
		<div>
		  <i>
			KIT 04.12.2023
		  </i>
		</div>
	  </div>

	  <div id="tree" class="step" data-rel-x="0" data-rel-y="900">
		<p class="center">
		  <img id="treeFig" src="figures/tree.svg"></img>
		</p>
	  </div>

	  <div id="GWPhysics" class="step" data-rel-x="-2000" data-rel-y="0">
		<h2>Part I: Gravitational Wave Physics<span class="orange">.</span></h2>
		<p>
		  <em>A new window into our universe.</em>
		  <ul>
			<li>Near-horizon black hole physics</li>
			<li>Internal structure of neutron stars</li>
			<li>Tests of GR and maybe even quantum gravity</li>
			<li>Dark matter (e.g. primordial black holes, axion clouds,...)</li>
			<li>Formation of large scale structures</li>
			<li>...</li>
		  </ul>
		</p>
		<p class="center">
		  <blockquote class="callout quote EN">
			I would like to mention astrophysics; in this field, the strange properties of the pulsars and quasars, and perhaps also the gravitational waves, can be considered as a challenge.
			<cite> - Werner Heissenberg</cite>
		  </blockquote>
		</p>
	  </div>

	  <div id="Collider" class="step" data-rel-x="2000" data-rel-y="0" data-rel-to="tree">
		<h2>Part II: C<span class="orange">o</span>llider Physics</h2>
		<p>
		  <em>A miscroscopic view at most fundamental structures of nature.</em><br/>
		  The standard model of particle physics may be well established, but what about
		  <ul>
			<li>Confinement?</li>
			<li>Neutrino Oscillations?</li>
			<li>Dark matter/energy?</li>
			<li>Hierarchy problem?</li>
			<li>Unification with gravity?</li>
			<li>...</li>
			<p class="center">
			  <blockquote class="callout quote EN">
				I think I can safely say that nobody understands quantum mechanics.
				<cite> - Richard Feynman </cite>
			  </blockquote>
			</p>
		  </ul>
		</p>
	  </div>

	  <div id="Meets" class="step" data-rel-x="0" data-rel-y="900" data-rel-to="tree">
		<h2>Part III: Black hole scattering, B2B, and c<span class="orange">o</span>mputational tools</h2>
		<div class="horizontal">
		<img id="scattering2Fig" src="figures/scattering.svg"></img>
		<p> vs. </p>
		<img id="orbit2Fig" src="figures/orbit.svg"></img>
		</div>
		<div class="center">
		  and<br/>
		  <img id="FeynmanFig" src="figures/Feynman.png"></img>
		</div>
	  </div>

	  <div id="GWPhysicsAgain" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="GWPhysics">
	  </div>

	  <div id="paramDetermination" class="step" data-rel-y="800" data-rel-to="GWPhysicsAgain">
		<h3><span class="h3span">Parameter determinati<span class="orange">o</span>n</span></h3>
		<div class="ref"><a href="https://arxiv.org/pdf/2111.03606.pdf">[GWTC-3 LIGO & Virgo]</a></div>
		<div class="center">
		  <img id="massContoursFig" src="figures/massContours.png"></img>
		</div>
	  </div>

	  <div id="bank" class="step" data-rel-y="800" data-rel-to="paramDetermination">
		<h3><span class="h3span">Wavef<span class="orange">o</span>rm template bank</span></h3>
		<div class="ref"><a href="https://arxiv.org/pdf/1904.01683.pdf">[Roulet et al. 2019]</a></div>
		<div class="center">
		  <img id="bankFig" src="figures/bank.png"></img>
		</div>
	  </div>
	  

	  <div id="GWPhysics2" class="step" data-rel-y="1000" data-rel-to="bank">
		<div class="center">
		  <img id="ligoFig" class="vanishPast" src="figures/ligo.png"></img>
		</div>
	  </div>

	  <div id="waveform1" class="step always" data-rel-z="-600" data-rel-to="GWPhysics2">
		<div class="center">
		  <img id="waveformFig" class="margin-bottom80" src="figures/waveform.svg"></img>
		</div>
	  </div>

	  <div id="waveform2" class="step" data-rel-to="waveform1">
		<h3 ><span class="h3span">Waveform m<span class="orange">o</span>delling</span></h3>
		<div class="margin-bottom450">
		<div class="ref"><a href="">[EOB: Buonanno, Damour 1998]</a></div>
		</div>
		<div class="center">
		  <img id="waveform2Fig" src="figures/waveform2.svg"></img>
		</div>
	  </div>

	  <div id="target" class="step" data-rel-y="1000" data-rel-z="600" data-rel-to="waveform2">
		<h3><span class="h3span">Target accuracy<span class="orange">.</span></span></h3>
		<div class="ref"><a href="https://journals.aps.org/prresearch/pdf/10.1103/PhysRevResearch.2.023151">[Pürrer, Haster 2020]</a></div>
		<div class="center">
		  <img id="mismatchFig" src="figures/mismatch.png"></img>
		  <ul>
		  <li>Mismatch needs to be reduced by three orders of magnitude for ET+CE</li>
		  </ul>
		</div>
	  </div>

	  <div id="ColliderAgain" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="Collider">
	  </div>

	  <div id="goals" class="step" data-rel-x="0" data-rel-y="850">
		<h3><span class="h3span">What d<span class="orange">o</span>&hairsp; we want?</span></h3>
		<p class="center"><em>Uncover structures, find patterns, and use them to our advantage!</em></p><br/>
		Examples
		<ul>
		  <li>Transcendentality: Iterated integrals.</li>
		  <li>Unitarity, singularities and branch cuts: Generalized unitarity, bootstrap.</li>
		  <li>Color-decomposition: color-ordered amplitudes.</li>
		  <li>Color-kinematics duality & the double copy.</li>
		</ul>
	  </div>

	  <div id="colorKinematics" class="step" data-rel-x="0" data-rel-y="650">
		<h3><span class="h3span">The C<span class="orange">o</span>lor-Kinematics Duality</span></h3>
		<a class="ref" href="https://arxiv.org/pdf/0805.3993.pdf">[0805.3993, BCJ]</a><br/>
		<p>
		  Consider a simple Yang-Mills amplitude
		  <div class="center">
			<div class="centerBox">
			  <img id="YMFig" src="figures/YM.svg"></img>
			</div>
		  </div>
		  The color factors fulfill relations like
		  <div class="center">
			<img id="cdEqnsFig" src="figures/cdEqns.png"></img>
		  </div>
		</p>
	  </div>

	  <div id="colorKinematics2" class="step" data-rel-x="0" data-rel-y="480">
		<p>
		 Challenge: Find a representation that fulfills
		  <div class="center">
		  \(
		  \begin{aligned}
		  c_i + c_j + c_k = 0 \quad &\Leftrightarrow \quad n_i + n_j +n_k = 0\\
		  c_i = -c_j \quad &\Leftrightarrow \quad n_i = -n_j
		  \end{aligned}
		  \)
		  </div>
		  But why?
		</p>
	  </div>

	  <div id="doubleCopy" class="step" data-rel-x="0" data-rel-y="500">
		<h3><span class="h3span">The Double C<span class="orange">o</span>py</span></h3>
		<a class="ref" href="https://arxiv.org/abs/1004.0476">[1004.0476, BCJ]</a><br/>
		<p>
		  Gravity amplitudes for free!
		  $$
		  \begin{aligned}
		  \cA_m^{(L)} &= i^{L-1}g_{\mathrm{YM}}^{m+2L-2} \sum_{\Gamma_i}\int \frac{\dd^{LD}\ell}{(2\pi)^{LD}}\frac{1}{S_i}\frac{n_i c_i}{D_i}\\
		  &\qquad\qquad\qquad\quad\Downarrow\\
		  \cM_m^{(L)} &= i^{L-1}\left(\frac{\kappa}{2}\right)^{m+2L-2} \sum_{\Gamma_i}\int \frac{\dd^{LD}\ell}{(2\pi)^{LD}}\frac{1}{S_i}\frac{n_i \tilde{n}_i}{D_i}
		  \end{aligned}
		  $$
		<div class="center">
			  <img id="dcStatesFig" src="figures/dcStates.png"></img>
			</div>
		</p>
	  </div>

	  <div id="doubleCopyExmpl" class="step" data-rel-x="0" data-rel-y="750">
		<hr/>
		<p>Some examples
		  <ul>
			<li><a class="refInline" href="https://arxiv.org/pdf/1804.09311.pdf">[1804.09311, Bern et al.]</a> \(\cN=8\) Supergravity @5 loops
			  <ul>
			    <li>Motivation: Study UV divergences of (supersymmetric) gravitational theories
				  <div class="center">
					<img id="uvDivFig" src="figures/uvDiv.png"></img>
				  </div>
				  <a class="ref" href="https://arxiv.org/pdf/2304.07392.pdf">[from 2304.07392, Bern et al.]</a><br/>
				</li>
				<li>Enhanced UV cancellations (counterterm exists, but no divergence):
				  <ul>
					<li>Pure half-maximal supergravity @2 loops in \(D=5\)</li>
					<li>Pure \(\cN = 4\) supergravity @3 loops in \(D=4\)</li>
					<li>\(\cN = 5\) supergravity @4 loops in \(D=4\)</li>
				  </ul>
				</li>
			  </ul>
			</li>
		  </ul>
		</p>
	  </div>

	  <div id="doubleCopyExmpl2" class="step" data-rel-x="0" data-rel-y="670">
		<p>
		  <ul>
			<li>From \(\cN=4\) Super-Yang-Mills to QCD and (super-)gravity
			  <ul>
				<li><a class="refInline" href="https://arxiv.org/abs/1407.4772">[1407.4772, Johansson, Ochirov]</a> SGR from \(\cN=0,1,2,4\) SQCD @1 loop</li>
				<li><a class="refInline" href="https://arxiv.org/abs/1706.09381">[1706.09381, w/ Johansson, Mogull]</a> \(\cN=4\) SGR from \(\cN=2\) SQCD @2 loops
				  <ul>
					<li>Color-kinematics duality + Supersymmetric decomposition	</li>
				  </ul>
				  <div class="center">
					<img id="decompNeq2Fig" src="figures/decompNeq2.png"></img>
				  </div>
				</li>
				<li><a class="refInline" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">[to appear, w/ Johansson]</a> \(\cN=1\) SQCD @2 loops
				  <div class="center">
					<img id="decompNeq1vFig" src="figures/decompNeq1v.png"></img>
					<img id="decompNeq1mFig" src="figures/decompNeq1m.png"></img>
				  </div>
				</li>
				<li>Ultimate goal: simple representations for QCD, and generic GR + matter amplitudes @high loops
			  </ul>
			</li>
		  </ul>
		</p>
	  </div>

	  <div id="Meets2" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="Meets">
	  </div>
	  
	  <div id="B2BOverview" class="step" data-rel-x="0" data-rel-y="900">
		<h3><span class="h3span">B2B <span class="orange">o</span>verview</span></h2>
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
	  </div>

      <div id="waveform2Again" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="waveform2">
	  </div>

	  <div id="B2BOverviewAgain" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="B2BOverview">
	  </div>

	  <!--
	  <div id="PNvsPM" class="step" data-rel-x="0" data-rel-y="820">
		<h3><span class="h3span">Post-Newto<span class="orange"></span>nion vs. Post-Minkowskian</span></h3>
		<p class="center">
		  PN expansion in \(v^2\sim \frac{Gm}{r}\) vs. PM expansion in \(G\)<br/>
		  State-of-the-art for spinless \(\Delta p\) (or \(\chi\)) including radiation-reaction effects
		</p>
		<p class="center">
		  <table class="tablePNPM">
			<thead>
			  <tr><th></th> <th></th> <th scope="col" class="complete">0PN</th> <th scope="col" class="complete">1PN</th> <th scope="col" class="complete">2PN</th> <th scope="col" class="complete">3PN</th> <th scope="col" class="complete">4PN</th> <th scope="col">5PN</th> <th scope="col">6PN</th> </tr>
			</thead>
			<tbody>
			  <tr class="complete"><th scope="row">0PM</th> <td>$$~~1~~$$</td> <td>$$v^2$$</td> <td>$$v^4$$</td> <td>$$v^6$$</td> <td>$$v^8$$</td> <td>$$v^{10}$$</td> <td>$$v^{12}$$</td> <td>$$v^{14}$$</td></tr>
			  <tr class="complete"><th scope="row">1PM</th> <td/> <td>$$1/r$$</td> <td>$$v^2/r$$</td> <td>$$v^4/r$$</td> <td>$$v^6/r$$</td> <td>$$v^8/r$$</td> <td>$$v^{10}/r$$</td> <td>$$v^{12}/r$$</td></tr>
			  <tr class="complete"><th scope="row">2PM</th> <td/> <td/> <td>$$1/r^2$$</td> <td>$$v^2/r^2$$</td> <td>$$v^4/r^2$$</td> <td>$$v^6/r^2$$</td> <td>$$v^8/r^2$$</td> <td>$$v^{10}/r^2$$</td> <td class="comment">Westpfahl '85</td></tr>
			  <tr class="complete"><th scope="row">3PM</th> <td/> <td/> <td/> <td>$$1/r^3$$</td> <td>$$v^2/r^3$$</td> <td>$$v^4/r^3$$</td> <td>$$v^6/r^3$$</td> <td>$$v^8/r^3$$</td> <td class="comment">cons: Bern et al. [1901.04424]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GK et al. [2007.04977]<br/>rad: Damour [2010.01641] + ...</td></tr>
			  <tr class="complete"><th scope="row">4PM</th> <td/> <td/> <td/> <td/> <td>$$1/r^4$$</td> <td>$$v^2/r^4$$</td> <td>$$v^4/r^4$$</td> <td>$$v^6/r^4$$</td> <td class="comment">cons: Bern et al. [2112.10750]<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dlapa et al. [2112.11296]<br/>rad: Dlapa et al. [2210.05541]</td></tr>
			  <tr><th scope="row">5PM</th> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete">$$1/r^5$$</td> <td>$$v^2/r^5$$</td> <td>$$v^4/r^5$$</td></tr>
			  <tr><th scope="row">6PM</th> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td class="complete"/> <td>$$1/r^6$$</td> <td>$$v^2/r^7$$</td></tr>
			</tbody>
		  </table>
		</p>
	  </div>
	  -->

	  <div id="B2B" class="step" data-rel-x="0" data-rel-y="800">
		<h3><span class="h3span">B<span class="orange">o</span>ndary-To-Bound (B2B)</span></h3>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a><br/>
		<p>
		  Let us do some classical mechanics! Consider 2-body Hamiltonian in com coordinates
		  $$\cH \equiv \cH(\bp^2,\br^2) = E \,.$$
		  Invert
		  $$\bp^2\equiv\bp^2(\br^2,E)\,.$$
		  Motion is in a plane \(\rightarrow\) polar coordinates
		  $$\bp = p_r \hat{\br} + \frac{p_\phi}{r} \hat{\boldsymbol\phi}\,.$$

		</p>
	  </div>

	  <div id="B2B2" class="step" data-rel-x="0" data-rel-y="480">
		<p>
		  Identify the angular piece with the conserved angular momentum \(J=p_\phi\), leading to
		  $$\bp^2(\br^2,E) = p_r^2(r,E,J)+\frac{J^2}{\br^2}\,.$$
		  Finally, use Hamilton's equations
		  $$\begin{aligned}
		  \Delta \phi
		    &= \int \dd \phi
		    = \int\frac{\dd\phi}{\dd r}\dd r
		    = \int\frac{\dot\phi}{\dot r}\dd r
		    = \int\left(\frac{\partial \cH}{\partial p_\phi}\right)/\left(\frac{\partial \cH}{\partial p_r}\right)\dd r\\
		    &= \int\left(\frac{\partial \cH}{\partial \bp^2}\frac{\partial\bp^2}{\partial J}\right)/\left(\frac{\partial \cH}{\partial \bp^2}\frac{\partial \bp^2}{\partial p_r}\right)\dd r
		    = \int \frac{J}{r^2 p_r}\dd r
		  \end{aligned}$$
		</p>
	  </div>

	  <div id="B2B3" class="step" data-rel-x="0" data-rel-y="420">
		<p>
		  Periastron advance:
		  <div class="center">
			<div class="centerBox">
			  $$\Delta \Phi + 2\pi =  2J \int_{r_-}^{r_+} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}$$
			</div>
		  </div>
		  Scattering angle (\(b=J/p_\infty\)):
		  <div class="center">
			<div class="centerBox">
			  $$\chi(J,E) +\pi = 2J \int_{r_{\rm min}}^\infty \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}$$
			</div>
		  </div>
		  It turns out that: \(r_-(J,E) = r_\mathrm{min}(J,E)\) and \(r_+(J,E) = r_\mathrm{min}(-J,E)\)
		</p>
	  </div>

	  <div id="B2B4" class="step" data-rel-x="500" data-rel-y="-55">
		$$\begin{align}
		&=2J \int_{r_\textrm{min}(J)}^{\infty} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}\\
		&\quad-2J \int_{r_\textrm{min}(-J)}^{\infty} \frac{\dd r}{r^2\sqrt{\bp^2(r,E)-J^2/r^2}}\\
		\end{align}$$
	  </div>

	  <div id="B2B5" class="step" data-rel-x="-500" data-rel-y="380">
		<p>
		  Boundary-to-bound for the periastron advance
		  <div class="center">
			<div class="centerBox">
			  $$\Delta\Phi(J,E) = \chi(J,E) + \chi(-J,E)$$
			</div>
		  </div>
		</p>
	  </div>

	  <div id="B2BOverviewAgain2" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="B2BOverview">
	  </div>

	  <div id="treeAgain2" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="tree">
	  </div>

	  <div id="treeAgainRev" class="step" data-rel-x="0" data-rel-y="0" data-rotate-z="180" data-rel-to="tree">
	  </div>

	  <div id="pmeft" class="step" data-rel-x="0" data-rel-y="-900" data-rotate-z="180" data-rel-to="tree">
		<h2>A w<span class="orange">o</span>rldline EFT framework</h2>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Porto 2006.01184]</a><br/>
		<ul>
		  <li>Model BHs/NSs as worldlines \(x_a^\mu(\tau)\) coupled to a gravitational field \(g_{\mu\nu}(x)\).</li>
		  <li>Perturbative expansion in \(G\): particle physics/amplitudes toolbox</li>
		  <li>EFT methodology: EFT-parametrized action \(\rightarrow\) effective action \(\rightarrow\) observables: scattering angle/change in angular momentum/fluxes/waveform/...</li>
		  <li>Complete: allows inclusion of radiation, finite size, spin, \(n\)-body</li>
		  <li>Classical: only compute tree-level diagrams.</li>
		</ul>
		<div class="center">
		  <img id="setupFig" src="figures/setup.svg"></img>
		</div>
	  </div>

	  <div id="pmeft2" class="step" data-rel-x="0" data-rel-y="-630" data-rotate-z="180" data-rel-to="pmeft">
		<h3><span class="h3span">Effective two-body acti<span class="orange">o</span>n</span></h3>
		First collider physics tool: Feynman graphs & rules!
		<p class="center">
		  $$g_{\mu\nu}=\eta_{\mu\nu} + \kappa h_{\mu\nu}$$
		  $$e^{i S_{\rm eff}[x_a] } = \int \cD h_{\mu\nu} \, e^{i S_{\rm EH}[h] + i S_{\rm pp}[x_a,h] + i S_{\rm GF}[h] + i S_{\rm TD}[h] }$$
		</p>
		Optimize the EH-Lagrangian by cleverly chosing gauge-fixing terms (GF) and adding total derivatives (TD):
		<div class="horizontalLeft">
		<ul>
		  <li>2-point Lagrangian: 2 terms</li>
		  <li>3-point Lagrangian: 6 terms</li>
		  <li>4-point Lagrangian: 18 terms</li>
		  <li>5-point Lagrangian: 36 terms</li>
		</ul>
		<img id="notBadFig" src="figures/notBad.png"></img>
		</div>
	  </div>

	  <div id="rad" class="step" data-rel-x="0" data-rel-y="-750" data-rotate-z="180" data-rel-to="pmeft2">
		<h3><span class="h3span">Computing the e<span class="orange">o</span>m</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Neef, Porto 2207.00580]</a>
		<p>We want to compute <em>in-in</em> observables: doubling of fields.<br/>
		Equation of motions:
		$$\left.\frac{\delta \cS_{\textrm{eff}}[x_+,x_-]}{\delta x_{b,-}^\mu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}} = 0 \quad\Longleftrightarrow\quad m_b \ddot{x}_b^\mu(\tau) = \left.-\eta^{\mu\nu}\frac{\delta \cS_{\textrm{eff,int}}[x_+,x_-]}{\delta x_{b,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}$$
		  </p>
		<ul class="margin-top60">
		  <li>Simple Feynman rules for <em>variation</em> of the action
			<ul>
			  <li>\(\textrm{Feynman}\rightarrow\textrm{retarded/advanced}\) propagator; graviton vertices unchanged</li>
			  <li>Source:
				<div class="graphsRad">
				  <img id="sourceFig" src="figures/source.svg" alt="source"></img>
				  <div>
					$$ = -\frac{i  m}{2\Mp}\int\dd\tau \, e^{i\, k\cdot x} v^\mu v^\nu$$
				  </div>
				</div>
			  </li>
			  <li>Unique sink (hit by variation):
				<div class="graphsRad">
				  <img id="sinkFig" src="figures/sink.svg" alt="source"></img>
				  <div>
					$$=-\frac{i m}{2\Mp}  \,e^{i\,k\cdot x} \left[ i\, k^\alpha v^\mu v^\nu-i\,k\cdot v\, \eta^{\mu\alpha}v^\nu-\eta^{\mu\alpha}\dot{v}^\nu-i\,k\cdot v\, \eta^{\nu\alpha}v^\mu-\eta^{\nu\alpha}\dot{v}^\mu\right]$$
				  </div>
				</div>
			  </li>
			</ul>
		  </li>
		</ul>
	  </div>

	  <div id="rad2" class="step" data-rel-x="0" data-rel-y="-630" data-rotate-z="180" data-rel-to="rad">
		<ul>
		  <li>1PM-3PM graphs
			<div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{1PM}=$$
		  </div>
		  <img id="graphsRad1Fig" src="figures/graphsRad1.svg" alt="graphsRad1"/>
    	<div class="marginLeft">
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{2PM}=$$
		  </div>
		  <img id="graphsRad2Fig" src="figures/graphsRad2.svg" alt="graphsRad2"/>
		  </div>
		  <div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_-^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{3PM}=$$
		  </div>
		  <img id="graphsRad3Fig" src="figures/graphsRad3.svg" alt="graphsRad3"/>
		  </div>
		  </li>
		</ul>
	  </div>
	  
	  <div id="pmeft3" class="step" data-rel-x="0" data-rel-y="-600" data-rotate-z="180">
		<h3><span class="h3span">C<span class="orange">o</span>mputing observables</span></h3>
		<p>
		  Solve eom perturbatively by expanding around straight lines
		  $$
		  x_a^\mu(\tau)=b_i^\mu+\tau u_i^\mu+\sum_n G^n \delta^{(n)}x_a^\mu(\tau).
		  $$
		Using above trajectories we can e.g. compute the deflection 
		$$\Delta p^\mu_1= m_1 \int_{-\infty}^{+\infty}\dd\tau \ddot{x}_1^\mu = - \eta^{\mu\nu}\int_{-\infty}^{+\infty} \dd\tau \left.\frac{\delta \cS_{\textrm{eff,int}}[x_-,x_+]}{\delta x_{1,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}\,,$$
		or the change of angular momentum
		$$\Delta J^{\mu\nu}_1 = m_1 \int_{-\infty}^{+\infty} \dd\tau ( x_1^\mu \ddot{x}_1^\nu- \ddot{x}_1^\mu x_1^\nu)\,.$$
		Alternatively, treat WLs as dynamical fields and directly compute classical expectation values, see work of Plefka et. al. (<em>WLEFT</em>).
		</p>
	  </div>

	  <div id="compSetup" class="step" data-rel-x="0" data-rel-y="-700" data-rotate-z="180">
		<h3 class="margin-bottom600"><span class="h3span">Computati<span class="orange">o</span>nal setup</span></h3>
		<img id="compSetupFig" src="figures/compSetup.svg"></img>
	  </div>

	  <div id="compSetup2" class="step" data-rel-x="0" data-rel-y="-700" data-rotate-z="180">
	  </div>

	  <div id="intProps" class="step" data-rel-x="-1600" data-rel-y="0" data-rotate-z="180">
		<h3><span class="h3span">Integral pr<span class="orange">o</span>perties</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2304.01275">[2304.01275 w/ Dlapa, Liu, Neef, Porto]</a>
		$$
		\int \dd^Dq\frac{\delta(q\cdot u_1)\delta(q\cdot u_2)e^{i b\cdot q}}{(q^2)^m}
		\underbrace{\int \dd^D\ell_1\cdots\dd^D\ell_L\frac{\delta(\ell_1\cdot u_{a_1})\cdots\delta(\ell_L\cdot u_{a_L})}{(\ell_1\cdot u_{b_1}\pm i0)^{i_1}\cdots(\ell_L\cdot u_{b_L}\pm i0)^{i_L}(\textrm{sq. props})}}_{\textrm{Cut Feynman integrals with linear and square propagators}}
		$$
		<ul>
		  <li>Single scale \(\gamma=u_1\cdot u_2\).</li>
		  <li>Uncut and cut linear propagators.</li>
		  <li>Function space up to 4PM: log, dilog, complete elliptic integrals (E and K).</li>
		  <li>Regions: @2PM potential; @3PM + rad1; @4PM +rad2; ...
			<div class="center horizontal">
			  <img id="I1fig" src="figures/I1.svg"></img>
			  <img id="I2fig" src="figures/I2.svg"></img>
			</div>
		  </li>
		  <li>Only for radiative (on-shell) propagators the <em>causal \(i0\)</em> matters.</li>
		</ul>
	  </div>

	  <div id="compSetup3" class="step" data-rel-x="0" data-rel-y="0" data-rotate-z="180" data-rel-to="compSetup2">
	  </div>

	  



	   <div id="Dpres" class="step" data-rel-x="0" data-rel-y="-800" data-rotate-z="180">
		<h3><span class="h3span">State-<span class="orange">o</span>f-the-art</span></h3>
		<ul>
		  <li>Impulse \(\Delta p_i^\mu\) (non-spinning)
			<ul>
			  <li>4PM potential <a class="refInline" href="https://arxiv.org/abs/2101.07254">[2101.07254 Bern et al.]</a><a class="refInline b" href="https://arxiv.org/abs/2106.08276">[2106.08276 Dlapa et al.]</a>[...]</li>
			  <li>4PM conservative <a class="refInline" href="https://arxiv.org/abs/2112.10750">[2112.10750 Bern et al.]</a><a class="refInline b" href="https://arxiv.org/abs/2112.11296">[2112.11296 Dlapa et al.]</a>[...]</li>
			  <li>4PM complete (+radiation) <a class="refInline b" href="https://arxiv.org/abs/2210.05541">[2210.05541 Dlapa et al.]</a><a class="refInline" href="https://arxiv.org/abs/2307.04746">[2307.04746 Damgaard et al.]</a><a class="refInline" href="https://arxiv.org/abs/2308.11514">[2308.11514 Jakobsen et al. (+spin)]</a></li>
			</ul>
		  </li>
		  <li>3PM change of angular momentum \(\Delta J\) (non-spinning): <a class="refInline" href="https://arxiv.org/abs/2203.04283">[2203.04283 Manohar et al.][...]</a>
		  </li>
		  <li>Waveforms
			<ul>
			  <li>LO: <a class="refInline" href="https://arxiv.org/abs/2101.12688">[2101.12688 Jakobsen et al.]</a><a class="refInline" href="https://arxiv.org/abs/2102.08339">[2102.08339 Mougiakakos et al.]</a>
			  </li>
			  <li>LO with spin:<a class="refInline" href="https://arxiv.org/abs/2106.10256">[2106.10256 Jakobsen et al.]</a><a class="refInline" href="https://arxiv.org/abs/2309.17429">[2309.17429 De Angelis et al.]</a> </li>
			  <li>NLO: <a class="refInline" href="https://arxiv.org/abs/2303.06111">[2303.03111 Brandhuber et al.]</a><a class="refInline" href="https://arxiv.org/abs/2303.06112">[2303.03112 Herderschee et al.]</a><a class="refInline" href="https://arxiv.org/abs/2303.07006">[2303.07006 Georgoudis et al.]</a><a class="refInline" href="https://arxiv.org/abs/2303.06211">[2303.06211 Elkhidir et al.]</a></li>
			</ul>
		  </li>
		</ul>
	  </div>
	  
	  <div id="Dpres2" class="step" data-rel-y="-750" data-rel-to="Dpres" data-rotate-z="180">
		<h3><span class="h3span">Firsov resummation </span></h3>
<div class="center">
		  <p class="inlineRef">Resummation and comparison to NR <a href="https://arxiv.org/abs/2211.01399">[Damour, Rettegno 2211.01399]</a></p>
		  <img id="chiEOBFig" src="figures/chiEOB.png"></img>
		</div>
	  </div>

	   <div id="conclusions" class="step" data-rel-x="0" data-rel-y="-750" data-rotate-z="180">
		<h2>Conclusi<span class="orange">o</span>ns</h2>
		<ul>
		  <li>Fruitful interaction between collider and gravitational wave physics.</li>
		  <li>CKD & DC & B2B: From colliding quarks to gravitational waves.</li>
		  <li>PMEFT & B2B: systematic and efficient framework to study the classical gravitational 2-body dynamics.</li>
		  <li>Modern integration techniques used for gravitational wave physics.</li>
		  <li>Resummed PM results agree (surprisingly?) well with numerical relativity simulations</li>
		</ul>
	  </div>

	  <div id="outlook" class="step" data-rel-x="0" data-rel-y="-550" data-rotate-z="180">
		<h2>Outlo<span class="orange">o</span>k</h2>
		<ul>
		  <li>We have not reached any tool's maximal capacity, but 5PM seems close to the limit.</li>
		  <ul>
			<li>How to beat the factorial growth of complexity? Solving IBPs is hard!</li>
		  </ul>
		  <li>Integration tools optimized for causal propagators.</li>
		  <li>Iterated elliptic integrals to be expected.</li>
		  <li>Completion of B2B for arbitrary spin and non-local contributions.</li>
		  <li>Combine CKD/DC with PMEFT/WLEFT.</LI>
		</ul>
	  </div>

	  <div id="final" class="step" data-rel-x="0" data-rel-y="-600" data-rotate-z="180">
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
		<p class="small">
		  This research is supported by the ERC-CoG “Precision Gravity: From the LHC to LISA” provided by the European Research Council (ERC) under the European Union’s H2020 research and innovation programme (grant No. 817791), by the DFG under Germany’s Excellence Strategy ‘Quantum Universe’ (No. 390833306).
		</p>
	  </div>

	  

	  <div id="overview1" class="step" data-scale="6" data-x="0", data-y="3300">
	  </div>

	  <div id="overview2" class="step" data-scale="6.5" data-x="0", data-y="-2700" data-rotate-z="180">
	  </div>

	  <div id="finalAgain" class="step" data-rel-x="0" data-rel-y="0" data-rotate-z="180" data-rel-to="final">
	  </div>

	  <div id="Firsov1" class="step" data-rel-x="0" data-rel-y="0" data-rotate-y="90" data-rotate-z="180">
		<h2>PM to PN: The Magic of Firsov</h2>
		<img id="firsovFig" src="figures/Firsov.jpg" alt="firsov">
		Let's consider a simple example
		$${\Delta \Phi}(j,\cE)= \sum_{n=1} \Delta \Phi_j^{(2n)}(\cE)/j^{2n}$$
		with \(j=J/(G M \mu)\). B2B states:
		$$\Delta \Phi_j^{(2n)}(\cE)= 4\, \chi^{(2n)}_j(\cE)$$
		Let's assume we don't know anything about \(\chi^{(4)}_j\). Does this mean we don't know anything about \(\Delta \Phi_j^{(4)}\)? There's lot of lower PN information that our PM results up to \(\chi_j^{(3)}\) should contain. Where are they?
	  </div>
	  
	  <div id="Firsov2" class="step" data-rel-x="0" data-rel-y="-680" data-rotate-y="90" data-rotate-z="180">
		<h3><span class="h3span">Firs<span class="orange">o</span>v's formula</span></h3>
		Let us invert
		$$\chi(b,E) = -\pi + 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}} = \sum_{n=1} \chi^{(n)}_b(E) \left(\frac{GM}{b}\right)^n $$
		<a class="inlineRef" href="">[Firsov '53]</a>: dependence on \(r_\textrm{min}\) drops out
		<div class="center"><div class="centerBox">
			$$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right] = 1 + \sum_{n=1}^\infty f_n(E) \left(\frac{GM}{r}\right)^n$$
		</div></div>
		These integrals are easy to perform in a PM-expanded form and one finds:
		$$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		The inversion thereof also exists.
	  </div>

	  <div id="Firsov3" class="step" data-rel-x="0" data-rel-y="-650" data-rotate-y="90" data-rotate-z="180">
		<h3><span class="h3span">Back to our pr<span class="orange">o</span>blem</span></h3>
		$$\frac{1}{4}\Delta \Phi_j^{(4)}= \chi^{(4)}_j = \left(\frac{p_\infty}{\mu}\right)^4\chi_b^{(4)}=\left(\frac{p_\infty}{\mu}\right)^4\frac{3\pi}{16}(2f_1f_3+f_2^2+2f_4)$$
		and in turn
		$$\begin{align}
		f_1&=2\chi_b^{(1)}\\
		f_2&=\frac{4}{\pi}\chi_b^{(2)}\\
		f_3&=\frac{1}{3}\left(\chi_b^{(1)}\right)^3+\frac{4}{\pi}\chi_b^{(1)}\chi_b^{(2)}+\chi_b^{(3)}
		\end{align}$$
		Perfectly reproduces 1 and 2PN information at order \(j^{-4}\).
	  </div>

	  <div id="DE" class="step" data-rel-x="0" data-rel-y="-900" data-rotate-y="90" data-rotate-z="180">
		<h3><span class="h3span">Differential equati<span class="orange">o</span>ns</span></h3>
		<p>
		  Use IBPs to write <a class="refInline" href="https://www.sciencedirect.com/science/article/abs/pii/037026939190536Y?via%3Dihub">[Kotikov '91]</a><a class="refInline" href="https://arxiv.org/abs/hep-th/9711188">[hep-th/971118 Remiddi]</a>
		  $$\partial_x \vec{f}(x,\epsilon) = A(x,\epsilon) \vec{f}(x,\epsilon)\,.$$
		  Transform \(\vec{g}(x,\epsilon)=T(x,\epsilon)\vec{f}(x,\epsilon)\) into <em>canonical</em> form <a class="refInline" href="https://arxiv.org/abs/1304.1806">[1304.1806 Henn]</a>
		  $$\partial_x \vec{g}(x,\epsilon) = \epsilon \tilde{A}(x) \vec{g}(x,\epsilon)\,.$$
		  Trivializes finding solution in expansion
		  $$\vec{g}(x,\epsilon) = \sum_k \epsilon^k \vec{g}^{(k)}(x)\,,$$
		  in terms of iterated integrals
		  $$\vec{g}^{(k)}(x)=\int_{x_0}^x \tilde{A}(x')\vec{g}^{(k-1)}(x')\dd x' + \vec{g}_0^{(k)}\,.$$
		</p>
	  </div>

	  <div id="numInt1" class="step" data-rel-x="0" data-rel-y="-900" data-rotate-y="90" data-rotate-z="180">
		<h3><span class="h3span">Numerical integrati<span class="orange">o</span>n with machine learning</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2209.01091">[Jinno, GK, Liu, Rubira 2209.01091]</a>
		<ul>
		  <li>Why numerical integration?</li>
		  <ul>
			<li>Cross-checks are important and incredibly useful</li>
			<li>Might be the only available method at higher loops</li>
			<li>Hybrid: Analytical bootstrap</li>
		  </ul>
		  <li>Need fast algorithms for high precision!</li>
		  <li>Our idea: teach a neural network to do the Monte-Carlo integration making use of the <em>normalizing flows</em> technology.</li>
		</ul>		
	  </div>

	  <div id="numInt2" class="step" data-rel-y="-550" data-rel-x="0" data-rotate-y="90" data-rotate-z="180">
		<ul>
		  <li><em>Importance Sampling:</em> Pick points for Monte-Carlo evaluation such that regions of large integrand \(|f|\) gain more weight.</li>
		  <li>I.e. take a distribution \(x(G)\), \(\dd G = g(x) \dd x\)
			$$I = \int_\Omega \dd x f(x) = \int_{\tilde\Omega} \dd G \frac{f(x(G))}{g(x(G))}$$
			that minimizes the variance
			$$\sigma^2=\frac{1}{N-1}\left[\frac{1}{N}\sum_i \left(\frac{f(G_i)}{g(G_i)}\right)^2-\left(\frac{1}{N}\sum_i \frac{f(G_i)}{g(G_i)}\right)^2\right]$$
		  </li>
		  <li>Hence, optimally \(g(x) = f(x)/I\), but it should also be invertible and fast to evaluate.</li>
		</ul>
	  </div>

	  <div id="numInt3" class="step" data-rel-y="-650" data-rel-x="0" data-rotate-y="90" data-rotate-z="180">
		<ul>
		  <li>Traditional Monte-Carlo algorithms assume \(g(x_1,\dots,x_n) = g(x_1) \dots g(x_n)\) and then use an \(N\)-dimensional histogram for each dimension.</li>
		  <ul>
			<li>Often not true at all and leads to a bad sampling.</li>
		  </ul>
		  <li>Better: Machine learning in the form of a neural network setup</li>
		  <div class="center">
			<img id="NNFig" src="figures/NN.png" alt="NN"/>
			$$J =\left|\begin{pmatrix} 1 & 0 \\ \frac{\partial C}{\partial m}\frac{\partial m}{\partial x_A} & \frac{\partial C}{\partial x_B}\end{pmatrix}\right| = \left| \frac{\partial C}{\partial x_B} \right|$$
		  </div>
		</ul>
	  </div>
	  
	   <div id="numInt4" class="step" data-rel-y="-800" data-rel-x="0" data-rotate-y="90" data-rotate-z="180">
		 <ul>
		   <li>Promising results for (potential) boundary integrals up to 4-loops</li>
		   <div class="center">
			 <img id="NNplots1Fig" src="figures/NNplots1.png" alt="NN1"/>
			 <img id="NNplots2Fig" src="figures/NNplots2.png" alt="NN2"/>
		   </div>
		 </ul>
	   </div>

	  <div id="plan" class="step" data-rel-x="0" data-rel-y="-900" data-rotate-z="180" data-rel-to="final">
		<h3><span class="h3span">Future Research Pr<span class="orange">o</span>jects</span></h3>
		<h4>IBP technologies & computational framework</h4>
		<div class="boxed margin-bottom30">
		  <ul>
			<li>Combine strengths of all approaches (Laporta/LiteRed/Groebner Basis/intersection theory).</li>
			<li>Develop a flexibel computational framework.</li>
			<li>Goal: 5PM and 6PM integral reduction.</li>
		  </ul>
		</div>
		<h4>Minimal color-decomposition in QCD</h4>
		<div class="boxed">
		  <ul>
			<li>Generalize <em>Mario World</em> construction to arbitrary loop order.</li>
			<li>New input from: <em>All Loop Scattering as a Counting Problem</em> by Arkani-Hamed et al.</li>
			<li>Goal: Optimize integrands wrt. integration.</li>
		  </ul>
		  <div class="center">
			<img id="marioFig" src="figures/mario.png"></img>
		  </div>
		</div>
	  </div>

	  <div id="plan2" class="step" data-rel-x="0" data-rel-y="-830" data-rotate-z="180">
		<h4>Numerical integration using machine learning</h4>
		<div class="boxed margin-bottom30">
		  <ul>
			<li>Understand the optimal setup of the neural network for (classes of) Feynman integrals.</li>
			<li>Develop efficient software.</li>
			<li>Goal: High precision results (for e.g. analytic reconstruction).</li>
		  </ul>
		</div>
		<h4>Boundary-to-bound and waveforms</h4>
		<div class="boxed margin-bottom30">
		  <ul>
			<li>Complete B2B.</li>
			<li>New results in the PM expansion.</li>
			<li>Novel resummation strategies for bound and unbound waveforms.</li>
			<li>Goal: Get ready for LISA/Einstein Telescope/Cosmic Explorer.</li>
		  </ul>
		</div>
		<h4>Towards QCD and its double copy</h4>
		<div class="boxed">
		  <ul>
			<li>Continue the SUSY decomposition program.</li>
			<li>Check CK-duality at high loops.</li>
			<li>Double copy for worldline formalism.</li>
			<li>Goal: Efficient methods for the construction of amplitudes in QCD and gravity.</li>
		  </ul>
		</div>
	  </div>
	  
  	</div>

	<div id="impress-toolbar"></div>

	<!--
	<div class="hint">
      <p>Use a spacebar or arrow keys to navigate. <br/>
		Press 'P' to launch speaker console.</p>
	</div>
	-->
	<script>
	  if ("ontouchstart" in document.documentElement) { 
		  document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
	  }
	  </script>

	<script src="js/impress.js"></script>
	<script>impress().init();</script>

  </body>
</html>
